<?php    
/*
 * PHP QR Code encoder
 *
 * Exemplatory usage
 *
 * PHP QR Code is distributed under LGPL 3
 * Copyright (C) 2010 Dominik Dzienia <deltalab at poczta dot fm>
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 3 of the License, or any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301 USA
 */
    
    echo "<h1>PHP QR Code</h1><br/>";
    
    //set it to writable location, a place for temp generated PNG files
    $PNG_TEMP_DIR = dirname(__FILE__).DIRECTORY_SEPARATOR.'temp'.DIRECTORY_SEPARATOR;
    
    //html PNG location prefix
    $PNG_WEB_DIR = 'temp/';

    include "qrlib.php";    
    
    //ofcourse we need rights to create temp dir
    if (!file_exists($PNG_TEMP_DIR))
        mkdir($PNG_TEMP_DIR);
    
    
    $filename = 'test.svg';
    
    //processing form input
    //remember to sanitize user input in real-life solution !!!
    $errorCorrectionLevel = 'L';
	
    if (isset($_REQUEST['level']) && in_array($_REQUEST['level'], array('L','M','Q','H')))
        $errorCorrectionLevel = $_REQUEST['level'];    

    $matrixPointSize = 4;
    if (isset($_REQUEST['size']))
        $matrixPointSize = min(max((int)$_REQUEST['size'], 1), 10);


    if (isset($_REQUEST['data'])) { 
    
        //it's very important!
        if (trim($_REQUEST['data']) == '')
            die('data cannot be empty! <a href="?">back</a>');
            
        // user data
        $filename = 'test'.md5($_REQUEST['data'].'|'.$errorCorrectionLevel.'|'.$matrixPointSize).'.svg';
        //QRcode::png($_REQUEST['data'], $filename, $errorCorrectionLevel, $matrixPointSize, 2);   

		$data = $_REQUEST['data'];	
        
    } else {    
    
        //default data
        echo 'You can provide data in GET parameter: <a href="?data=like_that">like that</a><hr/>';    
        //QRcode::png('PHP QR Code :)', $filename, $errorCorrectionLevel, $matrixPointSize, 2);  
		$data = "exemple";		
    }    
	
	// Saving SVG to file, demo with sourcecode preview 
     
    // here our data 
    $name         = 'John Doe'; 
    $sortName     = 'Doe;John'; 
    $phone        = '(049)012-345-678'; 
    $phonePrivate = '(049)012-345-987'; 
    $phoneCell    = '(049)888-123-123'; 
    $orgName      = 'My Company Inc.'; 

    $email        = 'john.doe@example.com'; 

    // if not used - leave blank! 
    $addressLabel     = 'Our Office'; 
    $addressPobox     = ''; 
    $addressExt       = 'Suite 123'; 
    $addressStreet    = '7th Avenue'; 
    $addressTown      = 'New York'; 
    $addressRegion    = 'NY'; 
    $addressPostCode  = '91921-1234'; 
    $addressCountry   = 'USA'; 

    // we building raw data 
    $codeContents  = 'BEGIN:VCARD'."\n"; 
    $codeContents .= 'VERSION:2.1'."\n"; 
    $codeContents .= 'N:'.$sortName."\n"; 
    $codeContents .= 'FN:'.$name."\n"; 
    $codeContents .= 'ORG:'.$orgName."\n"; 

    $codeContents .= 'TEL;WORK;VOICE:'.$phone."\n"; 
    $codeContents .= 'TEL;HOME;VOICE:'.$phonePrivate."\n"; 
    $codeContents .= 'TEL;TYPE=cell:'.$phoneCell."\n"; 

    $codeContents .= 'ADR;TYPE=work;'. 
        'LABEL="'.$addressLabel.'":' 
        .$addressPobox.';' 
        .$addressExt.';' 
        .$addressStreet.';' 
        .$addressTown.';' 
        .$addressPostCode.';' 
        .$addressCountry 
    ."\n"; 

    $codeContents .= 'EMAIL:'.$email."\n"; 

    $codeContents .= 'END:VCARD'; 

    // generating 
    QRcode::svg($data, $filename, $errorCorrectionLevel, 4, $matrixPointSize); 
     
    //$svgCodeFromFile = file_get_contents($filename); 
     
    // taken from: http://php.net/manual/en/function.highlight-string.php by: Dobromir Velev 
    function xml_highlight($s){ 
        $s = preg_replace("|<([^/?])(.*)\s(.*)>|isU", "[1]<[2]\\1\\2[/2] [5]\\3[/5]>[/1]", $s); 
        $s = preg_replace("|</(.*)>|isU", "[1]</[2]\\1[/2]>[/1]", $s); 
        $s = preg_replace("|<\?(.*)\?>|isU","[3]<?\\1?>[/3]", $s); 
        $s = preg_replace("|\=\"(.*)\"|isU", "[6]=[/6][4]\"\\1\"[/4]",$s); 
        $s = htmlspecialchars($s); 
        $s = str_replace("\t","&nbsp;&nbsp;",$s); 
        $s = str_replace(" ","&nbsp;",$s); 
        $replace = array(1=>'0000FF', 2=>'0000FF', 3=>'800000', 4=>'00AA00', 5=>'FF0000', 6=>'0000FF'); 
        foreach($replace as $k=>$v) { 
            $s = preg_replace("|\[".$k."\](.*)\[/".$k."\]|isU", "<font color=\"#".$v."\">\\1</font>", $s); 
        } 
        return nl2br($s); 
    } 

    // tag output 
    //echo $svgCodeFromFile; 
    //echo '<br/>'; 
	
    //display generated file
    //echo '<img src="'.$PNG_WEB_DIR.basename('test.svg').'" /><br/>';  
	
    // we print code 
	
    //echo '<span style="font-family: monospace, Courier, Courier New;font-size: 8pt">'; 
    //echo xml_highlight($svgCodeFromFile); 
    //echo '</span><br /><br />';
	

    // displaying 
    echo '<img src="'.$filename.'" />'; 
    
    //config form
    echo '<form action="index.php" method="post">
        Data:&nbsp;<input name="data" value="'.(isset($_REQUEST['data'])?htmlspecialchars($_REQUEST['data']):'PHP QR Code :)').'" />&nbsp;
        ECC:&nbsp;<select name="level">
            <option value="L"'.(($errorCorrectionLevel=='L')?' selected':'').'>L - smallest</option>
            <option value="M"'.(($errorCorrectionLevel=='M')?' selected':'').'>M</option>
            <option value="Q"'.(($errorCorrectionLevel=='Q')?' selected':'').'>Q</option>
            <option value="H"'.(($errorCorrectionLevel=='H')?' selected':'').'>H - best</option>
        </select>&nbsp;
        Size:&nbsp;<select name="size">';
        
    for($i=1;$i<=10;$i++)
        echo '<option value="'.$i.'"'.(($matrixPointSize==$i)?' selected':'').'>'.$i.'</option>';
        
    echo '</select>&nbsp;
        <input type="submit" value="GENERATE"></form><hr/>';
		

?>
<script> 

$(document).ready(function() {

	$('.tabs a').live('click', function(event) {

		select_tab($(this));

	});

	init_tabs();

})

</script>

<style type="text/css">

<!--

.style2 {font-size: x-small}

-->

</style>

</head>



<body>



<div class="container"><br>

	<div class="span-16 colborder">

	

		<form>

			

			<div>

				<b>Encodage</b>

				<select id="qrcode_encoding">

				<option>UTF-8</option>

				<option>Shift_JIS</option>

				<option>ISO-8859-1</option>

				</select>

				

				<span style="margin-left:20px;">

					<b>Taille </b><span class="style2">(max 500)</span>

					<input type="text" id="qrcode_width" value="300" style="width:60px;">

					X

					<input type="text" id="qrcode_height" value="300" style="width:60px;">

			  </span>

				

				<span style="margin-left:20px;">

					<b>Correction </b>

					<select id="qrcode_correction">

					<option>L</option>

					<option>M</option>

					<option>Q</option>

					<option>H</option>

					</select>

				</span>

				

			</div>

			

			<br>

			

			<div id="wrapper">

			

			    <ul class="tabs">

			        <li><a href="#text" class="current green" rel="text">Texte</a></li>

			        <li><a href="#email" class="green" rel="email">Email</a></li>

			        <li><a href="#tel" class="green" rel="tel">Tel</a></li>

			        <li><a href="#url" class="green" rel="url">Url</a></li>

			        <li><a href="#vcard" class="green" rel="vcard">vCard</a></li>

			        <li><a href="#mecard" class="green" rel="mecard">MeCard</a></li>

			        <li><a href="#sms" class="green" rel="sms">SMS</a></li>

			        <li><a href="#geo" class="green" rel="geo">Location</a></li>

			        <li><a href="#bookmark" class="green" rel="bookmark">Favoris</a></li>

			        <li><a href="#wifi" class="green" rel="wifi">WIFI</a></li>

			    </ul>

			 

			    <div class="tab_content" id="text">

			    	<div style="color:#666"><b>Texte</b></div>

			    	<textarea id="qrcode_text" style="width:90%; height:100px;"></textarea>

			    	<br><span id="nb_char"></span>

			    </div>

			    

			    <div class="tab_content" id="email">

			    	<div style="color:#666"><b>Email</b></div>

			    	<input type="text" id="qrcode_email" style="width:260px;"><br>

			    	<div style="color:#666"><b>Sujet</b></div>

			    	<input type="text" id="qrcode_subject" style="width:260px;"><br>

			    	<div style="color:#666"><b>Message</b></div>

			    	<textarea id="qrcode_message" style="width:90%; height:100px;"></textarea>

			    </div>

			    

			    <div class="tab_content" id="tel">

			    	<div style="color:#666"><b>T&eacute;</b><strong>l&eacute;phone</strong></div>

			    	<input type="text" id="qrcode_tel" style="width:260px;">

			    </div>

			    

			    <div class="tab_content" id="url">

			    	<div style="color:#666"><b>Url</b></div>

			    	<input type="text" id="qrcode_url" style="width:260px;">

			    </div>

			    

			    <div class="tab_content" id="vcard">

			    	<div class="span-8">

				    	<div style="color:#666"><b>Pr&eacute;nom</b></div>

				    	<input type="text" id="qrcode_vcard_fname" style="width:260px;">

				    	<div style="color:#666"><b>Nom</b></div>

				    	<input type="text" id="qrcode_vcard_lname" style="width:260px;">

				    	<div style="color:#666"><strong>Soci&eacute;t&eacute;</strong></div>

				    	<input type="text" id="qrcode_vcard_org" style="width:260px;">

				    	<div style="color:#666"><b>Fonction</b></div>

				    	<input type="text" id="qrcode_vcard_title" style="width:260px;">

				    	<div style="color:#666"><b>T&eacute;l&eacute;phone</b></div>

				    	<input type="text" id="qrcode_vcard_tel" style="width:260px;">

				    	<div style="color:#666"><b>Email</b></div>

				    	<input type="text" id="qrcode_vcard_email" style="width:260px;">

				    	<div style="color:#666"><b>Url de votre site </b></div>

				    	<input type="text" id="qrcode_vcard_url" style="width:260px;">

				    </div>

				    <div class="span-7 last">

				    	<div style="color:#666"><b>Rue</b></div>

				    	<input type="text" id="qrcode_vcard_street" style="width:260px;">

				    	<div style="color:#666"><b>Ville</b></div>

				    	<input type="text" id="qrcode_vcard_city" style="width:260px;">

				    	<div style="color:#666"><b>R&eacute;gion</b></div>

				    	<input type="text" id="qrcode_vcard_state" style="width:260px;">

				    	<div style="color:#666"><b>Code postal </b></div>

				    	<input type="text" id="qrcode_vcard_zip" style="width:260px;">

				    	<div style="color:#666"><b>Pays</b></div>

				    	<input type="text" id="qrcode_vcard_country" style="width:260px;">

				    	<div style="color:#666"><b>Notes</b></div>

				    	<textarea id="qrcode_vcard_note" style="width:260px; height:60px;"></textarea>

				    </div>

			    </div>

			    

			    <div class="tab_content" id="mecard">

			    	<div class="span-8">

				    	<div style="color:#666"><b>Pr&eacute;nom</b></div>

				    	<input type="text" id="qrcode_mecard_fname" style="width:260px;">

				    	<div style="color:#666"><b>Nom</b></div>

				    	<input type="text" id="qrcode_mecard_lname" style="width:260px;">

				    	<div style="color:#666"><b>T&eacute;l&eacute;phone</b></div>

				    	<input type="text" id="qrcode_mecard_tel" style="width:260px;">

				    	<div style="color:#666"><b>Email</b></div>

				    	<input type="text" id="qrcode_mecard_email" style="width:260px;">

				    	<div style="color:#666"><b>Url de votre site </b></div>

				    	<input type="text" id="qrcode_mecard_url" style="width:260px;">

				    </div>

				    <div class="span-7 last">

				    	<div style="color:#666"><b>Rue</b></div>

				    	<input type="text" id="qrcode_mecard_street" style="width:260px;">

				    	<div style="color:#666"><strong>Ville</strong></div>

				    	<input type="text" id="qrcode_mecard_city" style="width:260px;">

				    	<div style="color:#666"><b>R&eacute;gion</b></div>

				    	<input type="text" id="qrcode_mecard_state" style="width:260px;">

				    	<div style="color:#666"><b>Code postal</b></div>

				    	<input type="text" id="qrcode_mecard_zip" style="width:260px;">

				    	<div style="color:#666"><b>Pays	</b></div>

				    	<input type="text" id="qrcode_mecard_country" style="width:260px;">

				    	<div style="color:#666"><b>Notes</b></div>

				    	<textarea id="qrcode_mecard_note" style="width:260px; height:60px;"></textarea>

				    </div>

			    </div>

			    

			    <div class="tab_content" id="sms">

			    	<div style="color:#666"><b>T&eacute;l&eacute;phone</b></div>

			    	<input type="text" id="qrcode_sms_tel" style="width:260px;">

			    	<div style="color:#666"><b>Message</b></div>

			    	<textarea id="qrcode_sms_message" style="width:90%; height:100px;"></textarea>

			    </div>

			    

			    <div class="tab_content" id="geo">

			    	<div style="color:#666"><b>Addresse</b></div>

			    	<input type="text" id="qrcode_geo_address" style="width:300px;">

			    	<br>

			    	Ou<br>

			    	<div style="color:#666"><b>Latitude</b></div>

			    	<input type="text" id="qrcode_geo_lat" style="width:200px;">

			    	<div style="color:#666"><b>Longitude</b></div>

			    	<input type="text" id="qrcode_geo_lng" style="width:200px;">

			    </div>

			    

			    <div class="tab_content" id="bookmark">

			    	<div style="color:#666"><b>Titre</b></div>

			    	<input type="text" id="qrcode_bkm_title" style="width:260px;">

			    	<div style="color:#666"><b>Url du site </b></div>

			    	<input type="text" id="qrcode_bkm_url" style="width:260px;">

			    </div>

			    

			    <div class="tab_content" id="wifi">

			    	<div style="color:#666"><b>SSID</b></div>

			    	<input type="text" id="qrcode_wifi_ssid" style="width:260px;">

			    	<div style="color:#666"><b>Mot de passe </b></div>

			    	<input type="text" id="qrcode_wifi_password" style="width:260px;">

			    	<div style="color:#666"><b>Type</b></div>

			    	<select id="qrcode_wifi_type">

			    	<option value="WEP">WEP</option>

			    	<option value="WPA">WPA/WPA2</option>

			    	<option value="nopass" selected>Aucun</option>

			    	</select>

			    </div>

			    

			</div>

			<br>

			<hr class="space">

			<input type="submit" id="generate_qrcode_btn" class="green btn" value="G&eacute;n&eacute;rez !">

		</form>

	</div>
	<div class="span-7 last">

<b>R&eacute;sultat</b>

		<div id="qrcode_preview" style="margin-bottom:10px;"></div>

		<div id="url_box" style="margin-bottom:10px;"></div>

		<div id="short_url_box" style="margin-bottom:10px;"></div>

		<div id="download_box" style="margin-bottom:10px;"></div>

		<div id="sharing_box" style="margin-bottom:10px;"></div>
  </div>

	<?php

	

	?>

</div><?php
        
    // benchmark
    QRtools::timeBenchmark();    
	

    