<?php	function getDataFromUrl($url, $postParms='', $type='') {
		if($type=='json') {
			return json_curl_call($url, $postParms);
		}
		else {
			$ch = curl_init();
			$timeout = 5;
			curl_setopt($ch,CURLOPT_URL,$url);
			curl_setopt($ch,CURLOPT_RETURNTRANSFER,1);
			curl_setopt($ch,CURLOPT_CONNECTTIMEOUT,$timeout);
			//echo $postParms;
			$data = curl_exec($ch);
			curl_close($ch);
			return $data;
		}
	}
	function json_curl_call($url, $postParms='') {
		$ch = curl_init();
		curl_setopt($ch,CURLOPT_URL,$url);
		curl_setopt($ch,CURLOPT_POST,1);
		curl_setopt($ch,CURLOPT_POSTFIELDS,$postParms);
		curl_setopt($ch,CURLOPT_HTTPHEADER,array("Content-Type: application/json"));
	    curl_setopt($ch,CURLOPT_RETURNTRANSFER,1);
	    $data = curl_exec($ch);
	    curl_close($ch);
	    return $data;
	}
?>
