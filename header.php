<?php

    session_start();

    //set it to writable location, a place for temp generated PNG files
    $IMG_TEMP_DIR = 'phpqrcode'.DIRECTORY_SEPARATOR.'temp'.DIRECTORY_SEPARATOR;
    //html PNG location prefix
    $PNG_WEB_DIR = 'temp/';

    include "phpqrcode/qrlib.php";

    // ofcourse we need rights to create temp dir
    if (!file_exists($IMG_TEMP_DIR)) {
        mkdir($IMG_TEMP_DIR);
    }

	// Our configured function.
	suppression($IMG_TEMP_DIR , "svg" , "3600");
	suppression($IMG_TEMP_DIR , "png" , "3600");
	suppression($IMG_TEMP_DIR , "jpg" , "3600");

	// The function in question, removes too old files.
	function suppression($dossier_traite, $extension_choisie, $age_requis) {

        // We open the folder.
		$repertoire = opendir($dossier_traite);

		// We launch our loop that plays the files one by one.
        while(false !== ($fichier = readdir($repertoire))) {

            // The file path is put in a simple variable
            $chemin = $dossier_traite."/".$fichier;

            // Les variables qui contiennent toutes les infos nécessaires.
            $infos = pathinfo($chemin);
            $extension = $infos['extension'];

            $age_fichier = time() - filemtime($chemin);

            // On n'oublie pas LA condition sous peine d'avoir quelques surprises. :p
            if($fichier!="." AND $fichier!=".." AND !is_dir($fichier) AND
            	$extension == $extension_choisie AND $age_fichier > $age_requis) {
                unlink($chemin);
            }
        }
		closedir($repertoire); // On ferme !
	}

    // Function pour l'extention
    function extention($type_image) {
        switch ($type_image) {
            case IMAGETYPE_GIF:
                $ext_image = 'GIF';
                break;
            case IMAGETYPE_JPEG:
                $ext_image = 'JPEG';
                break;
            case IMAGETYPE_PNG:
                $ext_image = 'PNG';
                break;
            default:
                $ext_image = '';
                $mail_message = '<div class="col-lg-12">
                                    <div class="alert alert-dismissible alert-danger">
                                        <i class="fa fa-exclamation-circle"></i>
                                        <button type="button" class="close" data-dismiss="alert">X</button>
                                        <strong>ERREUR :</strong> Format d\'image non supporter !
                                    </div>
                                </div>';
                break;
        }
        return $ext_image;
    }

    $filename              = 'codeQR';
    $errorCorrectionLevel  = 'M';
	$data                  = "";
	$matrixPointSize       = 4;
	$size                  = 250;
	$encodage              = "";
	$type                  = "text";
	$extension             = "";
	$couleurBase           = "#000000";
	$couleur1              = $couleurBase;
	$couleur2              = $couleurBase;
	$forme                 = 'carre';
	$logo                  = "";
	$fond                  = "blanc";
	$error                 = '';

	if(isset($_REQUEST['generer'])
    && isset($_REQUEST['extension']) && isset($_REQUEST['couleur1'])
    && isset($_REQUEST['couleur2'])  && isset($_REQUEST['forme'])
    && isset($_REQUEST['type'])      && isset($_REQUEST['fond'])) {

		$type       = substr($_REQUEST['type'], 1);
		$extension  = $_REQUEST['extension'];
		$couleur1   = $_REQUEST['couleur1'];
		$couleur2   = $_REQUEST['couleur2'];
		$forme      = $_REQUEST['forme'];
		$logo       = $_REQUEST['logo'];
		$fond       = $_REQUEST['fond'];
        $error_form = FALSE;

        if ($forme === 'Carré')      { $forme = 'carre'; }
        if ($forme === 'Rond')       { $forme = 'rond'; }
        if ($fond === 'Transparent') { $fond = 'transparent'; }
        if ($fond === 'Blanc')       { $fond = 'blanc'; }

		if ($type == 'text') {
			//texte
			$text = $_REQUEST['texttext'];
			$data = $text;

            // Condition pour gerer les formulaire vide (ou parciellement vide)
            if($text === '') {
                $error_form = TRUE;
            }
		}

		if ($type == 'email') {
			//email
			$email   = $_REQUEST['emailemail'];
			$sujet   = $_REQUEST['sujetemail'];
			$message = $_REQUEST['messageemail'];

			$sujetencode = rawurlencode($sujet);
			$messageencode = rawurlencode($message);

			$data = "MATMSG:TO:".$email.";SUB:".$sujet.";BODY:".$message.";;";

            if(($email === '') || ($sujet === '') || ($message === '')) {
                $error_form = TRUE;
            }
		}

		if ($type == 'tel') {
			//tel
			$tel  = $_REQUEST['teltel'];
			$data = "tel:" . $tel;

            if($tel === '') {
                $error_form = TRUE;
            }
		}

		if ($type == 'url') {
			//url
			$url  = $_REQUEST['urlurl'];
			$data = $url;

            if($url === '') {
                $error_form = TRUE;
            }
		}

		if ($type == 'vcard') {
			// vcard
            $version = $_REQUEST['version'];
			$prenom  = $_REQUEST['prenomvcard'];
			$nom     = $_REQUEST['nomvcard'];
			$societe = $_REQUEST['societevcard'];
			$fonction = $_REQUEST['fonctionvcard'];
			$tel     = $_REQUEST['telvcard'];
			$email   = $_REQUEST['emailvcard'];
			$url     = $_REQUEST['urlvcard'];
			$notes   = $_REQUEST['notesvcard'];
			$rue     = $_REQUEST['ruevcard'];
			$ville   = $_REQUEST['villevcard'];
			$region  = $_REQUEST['regionvcard'];
			$postcode = $_REQUEST['postcodevcard'];
			$pays    = $_REQUEST['paysvcard'];

            // Vérification de la conformiter du logo

            if($logo !== '') {
                $type_image = exif_imagetype($logo);
                $ext_image = extention($type_image);
            }
            else {
                $ext_image = '';
            }

            // Version 2.1
            if($version === '1') {
                $data =
        			"BEGIN:VCARD"."\r\n".
        			"VERSION:2.1"."\r\n".
        			"N:".$nom.";".$prenom.";\n".
        			"FN:".$nom . ' ' . $prenom."\r\n".
        			"ORG:".$societe."\r\n".
        			"TITLE:".$fonction."\r\n".
                    "PHOTO;".$ext_image.":".$logo."\r\n".
        			"TEL;WORK;VOICE:".$tel."\r\n".
        			"ADR;WORK:;;".$rue.";".$ville.";".$region.";".$postcode.";".$pays."\r\n".
                    "LABEL;WORK;ENCODING=QUOTED-PRINTABLE:".$rue."=0D=0A".$ville.", ".$region." ".$postcode."=0D=0A".$pays."\r\n".
        			"EMAIL;PREF;INTERNET:".$email."\r\n".
        			"REV:".time()."\r\n".
        			"END:VCARD"
        			;
            }
            elseif($version === '2') {
                $date = date('Ymd').'T'.date('His').'Z';
                $data =
        			"BEGIN:VCARD"."\r\n".
        			"VERSION:3.0"."\r\n".
        			"N:".$prenom.";".$nom.";;\n".
        			"FN:".$nom." ".$prenom."\r\n".
        			"ORG:".$societe."\r\n".
        			"TITLE:".$fonction."\r\n".
                    "PHOTO;VALUE=URL;TYPE=".$ext_image.':'.$logo."\r\n".
        			"TEL;WORK;VOICE:".$tel."\r\n".
        			"ADR;TYPE=WORK:;;".$rue.";".$ville.";".$region.";".$postcode.";".$pays."\r\n".
                    "LABEL;TYPE=WORK:".$rue. ", ".$ville." ".$region.", ".$postcode." ".$pays."\r\n".
        			"EMAIL;TYPE=PREF;INTERNET:".$email."\r\n".
                    "REV:".$date."\r\n".
        			"END:VCARD"
        			;

            }
            elseif($version === '3') {
                $date = date('Y-m-d').'T'.date('H:i:s').'Z';
                $data =
        			"BEGIN:VCARD"."\r\n".
        			"VERSION:4.0"."\r\n".
        			"N:".$nom.";".$prenom.";;;\n".
        			"FN:".$prenom." ".$nom."\r\n".
        			"ORG:".$societe."\r\n".
        			"TITLE:".$fonction."\r\n".
                    "PHOTO;MEDIATYPE=image/".$ext_image.":".$logo."\r\n".
        			"TEL;TYPE=WORK,VOICE;VALUE=uri:tel:".$tel."\r\n".
        			"ADR;TYPE=WORK;LABEL=".$rue.", \n".$ville.", \n".$region.", \n".$postcode." ".$pays."\r\n".
        			"EMAIL:".$email."\r\n".
        			"REV:".$date."\r\n".
        			"END:VCARD"
        			;
            }
            else {
                $error_form = TRUE;
            }

            if(($prenom === '') || ($nom === '') || ($societe === '') || ($fonction === '') || ($tel === '') || ($email === '')
            || ($url === '') || ($pays === '') || ($rue === '') || ($ville === '') || ($region === '') || ($postcode === '')) {
                $error_form = TRUE;
            }
		}

		if($type == 'mecard') {
			//me card
			$prenom = $_REQUEST['prenommecard'];
			$nom = $_REQUEST['nommecard'];
			$tel = $_REQUEST['telmecard'];
			$email = $_REQUEST['emailmecard'];
			$url = $_REQUEST['urlmecard'];
			$rue = $_REQUEST['ruemecard'];
			$ville = $_REQUEST['villemecard'];
			$region = $_REQUEST['regionmecard'];
			$postcode = $_REQUEST['postcodemecard'];
			$pays = $_REQUEST['paysmecard'];
			$notes = $_REQUEST['notesmecard'];

			$data = "
    			MECARD:
    			N:".$nom.",".$prenom.";
    			TEL:".$tel.";
    			ADR:,,".$rue.",".$ville.",".$region.",".$postcode.",".$pays.";
    			EMAIL:".$email.";
    			URL:".$url.";
    			NOTE:".$notes.";
    			;
			    ";

            if(($prenom === '') || ($nom === '') || ($tel === '') || ($email === '') || ($url === '')
            || ($pays === '') || ($rue === '') || ($ville === '') || ($region === '') || ($postcode === '')) {
                $error_form = TRUE;
            }
		}

		if ($type == 'sms') {
			//sms
			$tel = $_REQUEST['telsms'];
			$message = $_REQUEST['messagesms'];

			$data = "SMSTO:".$tel.":".$message;

            if(($tel === '') || ($message === '')) {
                $error_form = TRUE;
            }
		}

		if ($type == 'geo') {
			//location
			//$adresse = $_REQUEST['adressegeo'];
			$lat = $_REQUEST['latgeo'];
			$lng = $_REQUEST['lnggeo'];

			if ($_REQUEST['latgeo'] != '' && $_REQUEST['lnggeo'] != '') {
				$data = "geo:".$lat.",".$lng;
			}

            if(($lat === '') || ($lng === '')) {
                $error_form = TRUE;
            }
		}

		if ($type == 'wifi') {
			//wifi
			$ssid = $_REQUEST['ssidwifi'];
			$mdp = $_REQUEST['mdpwifi'];
			$securite = $_REQUEST['securitewifi'];

			$data = "WIFI:S:".$ssid.";T".$securite.";P:".$mdp.";;";

            if(($ssid === '') || ($securite === '')) {
                $error_form = TRUE;
            }
		}

		if (isset($_REQUEST['size']) && $_REQUEST['size'] !="" && $_REQUEST['size'] <= 500) {
            $size = $_REQUEST['size'];
        }

		if (isset($_REQUEST['level']) && in_array($_REQUEST['level'], array('L','M','Q','H'))) {
            $errorCorrectionLevel = $_REQUEST['level'];
        }

		if (isset($_REQUEST['encodage'])) {
            $encodage = $_REQUEST['encodage'];
        }

		//it's very important!
		if ((trim($data) == '') || ($error_form === TRUE)) {
			$error = ('<div class="col-lg-12">
                            <div class="alert alert-dismissible alert-danger">
                                <i class="fa fa-exclamation-circle"></i>
                                <button type="button" class="close" data-dismiss="alert">X</button>
                                <strong>ERREUR :</strong> Les champs sont vide !
                            </div>
                        </div>');
    	}
		else {
            if($error_form === FALSE) {
                $_REQUEST['extension']  = strtolower($_REQUEST['extension']);
                $forme                  = strtolower($forme);
                $fond                   = strtolower($fond);

    			// user data
    			$filename = 'filename'.md5($data.'|'.$errorCorrectionLevel.'|'.$matrixPointSize).'.'. $_REQUEST['extension'];

    			// generator code qr
    			if($_REQUEST['extension'] === 'png') {
    				QRcode::png($data, $IMG_TEMP_DIR.$filename, $errorCorrectionLevel, $size, $matrixPointSize, $couleur1, $couleur2, $forme, $logo, $fond);
                }

    			else if($_REQUEST['extension'] == 'svg') {
    				QRcode::svg($data, $IMG_TEMP_DIR.$filename, $errorCorrectionLevel, 4, $size, $couleur1, $couleur2, $forme, $logo, $fond);
    			}

    			else if($_REQUEST['extension'] == 'jpg') {
    				QRcode::jpg($data, $IMG_TEMP_DIR.$filename, $errorCorrectionLevel, $size, $matrixPointSize, $couleur1, $couleur2, $forme, $logo);
    			}
            }
		}
}
?>
