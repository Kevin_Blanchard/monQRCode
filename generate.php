<?php if((isset($_REQUEST['generer'])) && (trim($data) != '') && ($error_form === FALSE)) { ?>
    <div class="container-fluid">
        <label class="col-lg-4 control-label">R&eacute;sultat :</label>
        <div id="qrcode_preview">
<?php
        // displaying
        echo '<iframe src="'.$IMG_TEMP_DIR.$filename.'" width="'.($size+$size/2).'" height="'.($size+$size/2).'" frameborder="0" scrolling="no"></iframe>';
?>
        </div>

        <div id="download_box">
            <a href="download.php?url=<?php echo $IMG_TEMP_DIR.$filename; ?>&extension=<?php echo $extension; ?>">
                <input type="button" class="col-lg-12 btn btn-lg btn-success" value="T&eacute;l&eacute;charger l'image">
            </a>
        </div>

        <div class="col-lg-12"><h2></h2></div>

        <div class="container-fluid">
            <form class="form-horizontal" method="post" action="mail.php">
                <div class="form-group">
                    <label class="col-lg-4 control-label">Envoyer par email :</label>
                    <div class="col-lg-8">
                        <input class="form-control" type="text" name="email" id="send_email" value="" placeholder="exemple@exemp.le">
                    </div>
                </div>

                <div class="form-group">
                    <div class="col-lg-12">
                        <a href="mail.php">
                            <input type="hidden" name="img_send" value="<?php echo $IMG_TEMP_DIR.$filename; ?>">
                            <input type="submit"  class="col-lg-12 btn btn-lg btn-success" value="Envoyer l'image par email">
                        </a>
                    </div>
                </div>
            </form>
        </div>
    </div>
<?php }
    else if($error != '') {
        echo $error;
    }

    if(isset($mail_message)) {
        echo $mail_message;
    }
?>
