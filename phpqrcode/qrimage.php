<?php
/*
 * PHP QR Code encoder
 *
 * Image output of code using GD2
 *
 * PHP QR Code is distributed under LGPL 3
 * Copyright (C) 2010 Dominik Dzienia <deltalab at poczta dot fm>
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 3 of the License, or any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301 USA
 */
 
 		function convertColor($color){
			#convert hexadecimal to RGB
			if(!is_array($color) && preg_match("/^[#]([0-9a-fA-F]{6})$/",$color)){

				$hex_R = substr($color,1,2);
				$hex_G = substr($color,3,2);
				$hex_B = substr($color,5,2);
				$RGB = hexdec($hex_R).",".hexdec($hex_G).",".hexdec($hex_B);

				return $RGB;
			}

			#convert RGB to hexadecimal
			else{
				if(!is_array($color)){$color = explode(",",$color);}

					foreach($color as $value){
					$hex_value = dechex($value); 
					if(strlen($hex_value)<2){$hex_value="0".$hex_value;}
					$hex_RGB.=$hex_value;
				}

				return "#".$hex_RGB;
			}

		}
		
	function imagecreatefromfile( $filename ) {
		if (!file_exists($filename)) {
			throw new InvalidArgumentException('File "'.$filename.'" not found.');
		}
		
		switch ( strtolower( pathinfo( $filename, PATHINFO_EXTENSION ))) {
			case 'jpeg':
			case 'jpg':
				return imagecreatefromjpeg($filename);
			break;

			case 'png':
				return imagecreatefrompng($filename);
			break;

			case 'gif':
				return imagecreatefromgif($filename);
			break;

			default:
				throw new InvalidArgumentException('File "'.$filename.'" is not valid jpg, png or gif image.');
			break;
		}
	}

    define('QR_IMAGE', true);
    class QRimage {
    
        //----------------------------------------------------------------------
        public static function png($frame, $filename = false, $pixelPerPoint = 4, $outerFrame = 4, $color1 = '#000000',$color2 ='#000000', $forme= 'carre', $logo="",$fond="blanc",$saveandprint=FALSE) 
        {
            $image = self::image($frame, $pixelPerPoint, $outerFrame, $color1, $color2, $forme, $logo, $fond);
			
            if ($filename === false) {
                Header("Content-type: image/png");
                ImagePng($image);
            } else {
                if($saveandprint===TRUE){
                    ImagePng($image, $filename);
                    header("Content-type: image/png");
                    ImagePng($image);
                }else{
                    ImagePng($image, $filename);
                }
            }
            
            ImageDestroy($image);
        }
        //----------------------------------------------------------------------
        public static function jpg($frame, $filename = false, $pixelPerPoint = 4, $outerFrame = 4, $color1 = '#000000',$color2 ='#000000', $forme= 'carre', $logo="",$saveandprint=FALSE) 
        {
            $image = self::image($frame, $pixelPerPoint, $outerFrame, $color1, $color2, $forme, $logo );
            
            if ($filename === false) {
                Header("Content-type: image/jpeg");
                ImageJpeg($image);
            } else {
                if($saveandprint===TRUE){
                    ImageJpeg($image, $filename, 100);
                    header("Content-type: image/jpeg");
                    ImageJpeg($image, NULL, 100);
                }else{
                    ImageJpeg($image, $filename, 100);
                }
            }
            
            ImageDestroy($image);
        }

        //----------------------------------------------------------------------
        public static function svg($frame, $filename = false, $outerFrame = 4, $size=250, $color1 = '#000000', $color2 = '#000000', $forme = 'carre', $logo="", $fond = "blanc")
        {
            $file = self::imageSVG($frame, $outerFrame, $size, $color1, $color2, $forme, $logo,$fond);
			//print_r ($frame);
			//echo ('<br />'.$color);
            if ($filename !== false) {
                $fhandle = fopen($filename,'w+');
                fwrite($fhandle, $file);
                fclose($fhandle);
            }
            unset($file);
        }
        //----------------------------------------------------------------------
        private static function image($frame, $pixelPerPoint = 4, $outerFrame = 4, $color1 = '#000000',$color2 ='#000000', $forme= 'carre', $logo="",$fond = "blanc") 
        {

            $h = count($frame);
            $w = strlen($frame[0]);
			
			$pixelPerPoint = $pixelPerPoint/$h;
            
            $imgW = $w + 2*$outerFrame;
            $imgH = $h + 2*$outerFrame;
			
			$size = $pixelPerPoint;
			
			$imgSizedW = $imgW*$size;
			$imgSizedH = $imgH*$size;
			
			$logoW = number_format($w/5,0);
			$logoH = number_format($h/5,0);
			
			$bordureLogo = ($logoW*$size)/5;
			
			$logoSizedW = ($logoW*$size) - $bordureLogo;
			$logoSizedH = ($logoH*$size) - $bordureLogo;
			$positionLogoX = ((($imgW/2) -($logoW/2))*$size)+ $bordureLogo;
			$positionLogoY = ((($imgH/2) -($logoH/2))*$size)+ $bordureLogo;
			
			$RGB1 = explode(",",convertColor($color1));
			$RGB2 = explode(",",convertColor($color2));
            
            $base_image =ImageCreateTrueColor($imgW, $imgH);
            
			
            $col[0] = ImageColorAllocate($base_image,255,255,255);
            $col[1] = ImageColorAllocate($base_image,0,0,0);
			$col[2] = ImageColorAllocate($base_image,$RGB1[0],$RGB1[1],$RGB1[2]);
			$col[3] = ImageColorAllocate($base_image,$RGB2[0],$RGB2[1],$RGB2[2]);
			
			$col[4] = imagecolorallocatealpha($base_image, 0, 0, 0, 127);
			
			if ($fond == "blanc"){
				imagefill($base_image, 0, 0, $col[0]);
			}
			else if( $fond == "transparent"){
				imagefill($base_image, 0, 0, $col[4]);
			}
			imagesavealpha($base_image, TRUE);
            
            for($y=0; $y<$h; $y++) {
                for($x=0; $x<$w; $x++) {
                    if ($frame[$y][$x] == '1') {
						if ( ($y < 8 && $x < 8 ) || $y < 8 && $x > $w-8 || $y > $h-8 && $x < 8 ){$color = $col[3];}
						else {$color = $col[2]; } 
                        ImageSetPixel($base_image,$x+$outerFrame,$y+$outerFrame,$color); 
                    }
                }
            }
			
            $target_image =ImageCreate($imgW * $pixelPerPoint, $imgH * $pixelPerPoint);
            ImageCopyResampled($target_image, $base_image, 0, 0, 0, 0, $imgW * $pixelPerPoint, $imgH * $pixelPerPoint, $imgW, $imgH);
            ImageDestroy($base_image);
            
            return $target_image;
        }
        //----------------------------------------------------------------------
        private static function imageSVG($frame, $outerFrame = 4, $size = 250, $color1 = '#000000', $color2 = '#000000', $forme = 'carre', $logo ="", $fond = "blanc")
        {
            $h = count($frame);
            $w = strlen($frame[0]);
			
			$size = $size/$h;
            
            $imgW = $w + 2*$outerFrame;
            $imgH = $h + 2*$outerFrame;
			
			$imgSizedW = $imgW*$size;
			$imgSizedH = $imgH*$size;
			
			$logoW = number_format($w/5,0);
			$logoH = number_format($h/5,0);
			
			$bordureLogo = ($logoW*$size)/5;
			
			$logoSizedW = ($logoW*$size) - $bordureLogo;
			$logoSizedH = ($logoH*$size) - $bordureLogo;
			$positionLogoX = ((($imgW/2) -($logoW/2))*$size)+ $bordureLogo;
			$positionLogoY = ((($imgH/2) -($logoH/2))*$size)+ $bordureLogo;
			
			$eps_string = "<svg xmlns=\"http://www.w3.org/2000/svg\" xmlns:xlink=\"http://www.w3.org/1999/xlink\" version=\"1.1\" width=\"".$imgSizedW."px\" height=\"".$imgSizedH."px\">\r";
            if( $fond == "blanc"){
				$eps_string .= "<rect x=\"0\" y=\"0\" width=\"".$imgW*$size."\" height=\"".$imgH*$size."\" style=\"fill:white\"/>\r";
			}
			for($y=0; $y<$h; $y++) {
                for($x=0; $x<$w; $x++) {
                    if ($frame[$y][$x] == '1') {
					
						if ( ($y < 8 && $x < 8 ) || $y < 8 && $x > $w-8 || $y > $h-8 && $x < 8 ){$color = $color2;}
						else if( $y+$outerFrame >= (($imgH/2) -($logoH/2)) AND $y+$outerFrame <= (($imgH/2) + ($logoH/2)) AND
								 $x+$outerFrame >= (($imgW/2) -($logoW/2)) AND $x+$outerFrame <= (($imgW/2) + ($logoW/2)) AND
								 $logo != "" ){$color = "#FFFFFF" ; }
						else {$color = $color1 ; } 
						
						if ( $forme == 'carre' ){
							$eps_string .= "<rect x=\"".(($x+$outerFrame)*$size)."\" y=\"".(($y+$outerFrame)*$size)."\" width=\"".$size."\" height=\"".$size."\" style=\"fill:".$color."\"/>\r";
						}
						else if ( $forme == 'rond' ){
							$eps_string .= " <circle cx=\"".((($x+$outerFrame)*$size)+($size/2))."\" cy=\"".((($y+$outerFrame)*$size)+($size/2))."\" r=\"".($size/2)."\" style=\"fill:".$color."\"/>\r";
						}
                    }
                }
            }
			if ( $logo != ""){
				//$eps_string .= "<rect x=\"".$positionLogoX."\" y=\"".$positionLogoY."\" height=\"".$logoSizedH."\" width=\"".$logoSizedW."\" style=\"fill:"."#FFFFFF"."\"/>\r";
				$eps_string .= "<image x=\"".$positionLogoX."\" y=\"".$positionLogoY."\" height=\"".$logoSizedH."\" width=\"".$logoSizedW."\" xlink:href=\"".$logo."\">\r</image>\r";
			}
            $eps_string .= "</svg>";
			
            return $eps_string;
        }
    }