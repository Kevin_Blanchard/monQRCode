/* jQuery app */
$(document).ready(function () {
    // Colorpicker
	'use strict';

	$('#menu-action li.active a').attr('aria-expanded', 'true');
	var id = $('#type').val();
	if (id === '') {
		id = '#text';
	}

	$(id).addClass('active');

	// Si le type de QRcode change, change la valeur de l'input
	$('#menu-action a[data-toggle="tab"]').on('shown.bs.tab', function (e) {
		$('#type').val(e.target.hash);
	});

	// Nombres de caractère : CHAMP TEXT
	$('#qrcode_text').keyup(function () {
		var nombreCaractere = $(this).val().length;
		$('#nb_char').text(nombreCaractere);
	});

	// Nombres de caractère : CHAMP SMS
	$('#qrcode_sms_message').keyup(function () {
		var nombreCaractere = $(this).val().length;
		$('#nb_char_sms').text(nombreCaractere);
	});

	$(function () {
		// Code for docs demos
		function createColorpickers (element) {
			// Api demo
			var bodyStyle = $(element);
			var Helement = element.substring(1);
			$(element).colorpicker({
				format: 'hex',
				color: bodyStyle.backgroundColor,
				colorSelectors: {
					'#777777': '#777777',
					'#337ab7': '#337ab7',
					'#5cb85c': '#5cb85c',
					'#5bc0de': '#5bc0de',
					'#f0ad4e': '#f0ad4e',
					'#d9534f': '#d9534f'
				},
				customClass: 'colorpicker-2x',
				sliders: {
					saturation: {
						maxLeft: 200,
						maxTop: 200
					},
					hue: {
						maxTop: 200
					},
					alpha: {
						maxTop: 200
					}
				}
			}).on('changeColor', function (ev) {
				document.getElementById(Helement).style.backgroundColor = ev.color.toHex();
			});
		}

		// Pour éviter de perdre la couleur lors d'un rechargement de page
		document.getElementById('qrcode_color1').style.backgroundColor = document.getElementById('qrcode_color1').value;
		document.getElementById('qrcode_color2').style.backgroundColor = document.getElementById('qrcode_color2').value;

		createColorpickers('#qrcode_color1');
		createColorpickers('#qrcode_color2');
	});
});
