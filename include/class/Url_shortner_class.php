<?php

class Url_shortner_class {
	
	function shorten_url($criteria=array()) {
		$url = $criteria['url'];
		$type = $criteria['type']; //google
		
		if($type=='google') {
			return $this->shorten_url_with_google(array('url'=>$url));
		}
	}
	
	/*
	Google shortner service
	*/
	function shorten_url_with_google($criteria=array()) {
		$url = $criteria['url'];
		
		$api_url = 'https://www.googleapis.com/urlshortener/v1/url';
		if($GLOBALS['google_api_key']!='') $api_url .= '?key='.$GLOBALS['google_api_key'];
		
		$result = getDataFromUrl($api_url, json_encode(array("longUrl"=>$url)), 'json');
		$result = json_decode($result,true);
		
		$data['longUrl'] = $url;
		$data['shortUrl'] = $result['id'];
		
		return $data;
	}
	
}

?>