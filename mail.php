<?php

    /* ### CONFIGURATION ##################### */
    $website = "http://exemple.com/";
    $you_email = "exemple@exemple.com";
    /* ####################################### */

    $ip_a_bannir = Array ('127.0.0.1', 'localhost', gethostname());

    $boundary = "-----=".md5(rand());

    // Chargement des variables postés
    $img   = $_POST['img_send'];
    $email = $_POST['email'];

    // HTML pour les messages d'erreur
    $pre_error = '<div class="col-lg-12">
                        <div class="alert alert-dismissible alert-danger">
                            <i class="fa fa-exclamation-circle"></i>
                            <button type="button" class="close" data-dismiss="alert">X</button>
                ';
    $post_error = '</div></div>';

    // Test de l'adresse email
    if(!filter_var($email, FILTER_VALIDATE_EMAIL)) {
        $mail_message = $pre_error.'Cet email a un format non adapté.'.$post_error;
    }

    if(!(stripos($email, '@') < 64) && !(stripos($email, '@') > 3)) {
        $mail_message = $pre_error.'Votre email est invalide.'.$post_error;
    }

    // On filtre les serveurs qui rencontrent des bogues.
    if (!preg_match("#^[a-z0-9._-]+@(hotmail|live|msn).[a-z]{2,4}$#", $email)) {
        $passage_ligne = "\r\n";
    }
    else {
        $passage_ligne = "\n";
    }

    // Rédaction du contenu du mail
    $content_email  = "Content-Type: text/plain; charset=\"UTF-8\"" . $passage_ligne;

    $content_email .= "Bonjour, voici en pièce jointe, le QRCode que vous avez créer sur $website." . $passage_ligne;

    // Dans le cas où nos lignes comportent plus de 70 caractères, nous les coupons en utilisant wordwrap()
    $content_email = wordwrap($content_email, 70, "\r\n");

    // Sujet
    $subject = 'Votre QRCode de ' . $website;

    // Entête
    $header  = 'From: '. $you_email                     . $passage_ligne;
    $header .= 'Reply-To: '. $email                     . $passage_ligne;
    $header .= 'X-Mailer: PHP/' . phpversion()          . $passage_ligne;
    $header .= "MIME-Version: 1.0"                      . $passage_ligne;
    $header .= "X-Priority: 3"                          . $passage_ligne;
    $header .= "Content-Type: multipart/alternative;"   . $passage_ligne;
    $header .= " boundary=\"$boundary\""                . $passage_ligne;

    if((!isset($mail_message) && file_exists($img))) {
        // ========== Lecture et mise en forme de la pièce jointe ===
        $fichier   = fopen($img, "r");
        $attachement = fread($fichier, filesize($img));
        $attachement = chunk_split(base64_encode($attachement));
        fclose($fichier);
        // ==========================================================

        //===== Ajout de la pièce jointe ============================
        $pieces  = "Content-Type: image/jpeg; name=\"$img\""            . $passage_ligne;
        $pieces .= "Content-Transfer-Encoding: base64"                  . $passage_ligne;
        $pieces .= "Content-Disposition: attachment; filename=\"$img\"" . $passage_ligne;
        $pieces .= $passage_ligne . $attachement . $passage_ligne       . $passage_ligne;
        $pieces .= $passage_ligne . "--" . $boundary . "--"             . $passage_ligne;
        // ==========================================================

        // Rattachement du message texte aux pièces jointes.
        $content_email = $content_email . $pieces;

        if (!isset($mail_message)) {
            // Envoi de l'email
            $retour = mail($email, $subject, $content_email, $header);
        }

        if($retour === TRUE) {
            $pre_error = '<div class="col-lg-12">
                                <div class="alert alert-dismissible alert-success">
                                    <i class="fa fa-exclamation-circle"></i>
                                    <button type="button" class="close" data-dismiss="alert">X</button>
                        ';
            $post_error = '</div></div>';
            $mail_message = $pre_error.'Votre QRCode vous a été envoyer par email.'.$post_error;
        }
        else {
            $mail_message = $pre_error.'Votre email est invalide.'.$post_error;
        }

    }
    else {
        $mail_message = $pre_error.'Votre email est invalide.'.$post_error;
    }

    include('index.php');
?>
