// on initialise 2 variables qui nous permettrons d'envoyer la couleur dans le bon champ.
champ = '';
formulaire = '';

function valid_couleur (couleur) {
    //fonction appelée lorsqu'on valide la palette. On récupère la couleur.
    document.forms[formulaire].elements[champ].value = couleur;
    document.forms[formulaire].elements[champ].style.backgroundColor = couleur;
}

function changementExtension () {
    if (document.getElementById('qrcode_extension').value === 'png') {
		$('.png').fadeIn('fast');
		$('.svg').fadeOut('fast');
		$('.jpg').fadeOut('fast');
	}
	else if (document.getElementById('qrcode_extension').value === 'svg') {
		$('.svg').fadeIn('fast');
		$('.png').fadeOut('fast');
		$('.jpg').fadeOut('fast');
	}
	else if (document.getElementById('qrcode_extension').value === 'jpg') {
		$('.jpg').fadeIn('fast');
		$('.svg').fadeOut('fast');
		$('.png').fadeOut('fast');
	}
}

window.onload = changementExtension;
